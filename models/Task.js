const mongoose = require("mongoose");

const taskSchema = new mongoose.Schema({
    name:  {
        type: String,
        required: [true, "Task name is required"]
    }, 
    status: {
        type: String,
        default: "Pending"
    }

});

// module.exports will allows us to export files/functions and be able to import/require them in another file within our application.
// files/functions of Task.js will be use in the controllers folder.
const Task = mongoose.model("Task", taskSchema);

module.exports = Task;