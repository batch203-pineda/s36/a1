// require the installed modules
const express = require("express");
const mongoose = require("mongoose");

// port
const port = 4000;

// server
const app = express();

// MongoDB connection
mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.bculsyb.mongodb.net/b203_to-do?retryWrites=true&w=majority",
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });

    let db = mongoose.connection;

    db.on("error", console.error.bind(console, "connection error"));

    db.once("open", () => console.log("We're connected to the cloud database."));

// middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));



// Routes Grouping - organize the access for each resources.
const taskRoutes = require("./routes/taskRoutes")

app.use("/tasks", taskRoutes);


/*

            Separation of Concerns

            Models Folder

            - Contain the Object Schemas and defines the object structure and content
            
            
            Controllers Folder

            - Contain the functions and business logic of our Express JS application
            - Meaning all the operations it can do will be placed in this file
            
            
            Routes Folder
            
            - Contains all the endpoints for our application
            - We separate the routes such that "index.js" only contains information on the server
           

            require -> to include a specific module/package.
            export -> to treat a value as a "module" that can be used by other files


            Flow of exports and require:
            export models > require in controllers
            export controllers > require in routes
            export routes > require in app.js


        */

// port listener
app.listen(port, () => console.log(`Server is running at port ${port}`));
