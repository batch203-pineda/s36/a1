const express = require("express");

// express.Router() method allows access to HTTP method
const router = express.Router();

const taskControllers = require("../controllers/taskControllers");

console.log(taskControllers);

// Create task routes
router.post("/", taskControllers.createTask)

// View all task route

router.get("/allTasks", taskControllers.getAllTasksController)

// Get a Single Task
router.get("/getSingleTask/:taskId", taskControllers.getSingleTaskController)

// Update task status
router.patch("/updateTask/:taskId", taskControllers.updateTaskStatusController);

// Delete a task
router.delete("/deleteTask/:taskId",
taskControllers.deleteTaskController)

// this will be use in our server.
module.exports = router;